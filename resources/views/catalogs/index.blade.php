@extends('layouts.app')

@section('content')
    <div class="container ">
        <div class="row">
            <div class="col-sm-8 tags">
                <div class="header">
                    <h1>Создайте новый каталог</h1>
                </div>

                <form id="formAdd" action="{{ route('catalogs.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="nameCatalog">Название каталога</label>
                        <input type="text" name="title" class="form-control" id="nameCatalog">
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="fileCatalog">Файл каталога</label>
                                <input type="file" name="file" multiple class="form-control-file" id="fileCatalog">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="imageCatalog">Картинка каталога</label>
                                <input type="file" name="image" multiple class="form-control-file" id="imageCatalog">
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Создать</button>
                </form>
            </div>
        </div>


        <hr>
        &nbsp;
        @include('errors')

        <div class="container">
            <table class="table">

                <thead class="thead-dark">

                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название каталога</th>
                    <th scope="col">Картинка каталога</th>
                    <th scope="col">Действия</th>
                </tr>

                </thead>
                <tbody>
                <?php $num = 1; ?>
                @foreach($catalogs as $catalog)
                <tr>
                    <th scope="row">{{$num++}}</th>
                    <td>{{$catalog->name}}</td>
                    @if($catalog->imageable['name'] == null)
                        <td>
                            <span>Not found</span>
                        </td>
                    @else
                        <td>
                            <img src="{{ asset('storage/catalogs/' . $catalog->imageable['name']) }}"
                                 alt="image"
                                 style="width: 100px;height: 100px">
                        </td>
                    @endif
                    <td>
                        <form action="{{ route('catalogs.destroy' , $catalog->id ) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button><i class="far fa-trash delete" aria-hidden="true"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach

                </tbody>

            </table>
        </div>
    </div>
@endsection
