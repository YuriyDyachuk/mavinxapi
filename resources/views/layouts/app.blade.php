<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Styles css and head pge -->
    @include('layouts._includes.head')
</head>
<body>
    <section>
        <div id="app">
            <div class="container">

                @yield('content')

                @include('layouts._includes.footer')
            </div>
        </div>
    </section>

    @yield('scripts')

    @stack('scripts')

</body>
</html>
