<link rel="stylesheet" type="text/css" href="{{ url('bower-components/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('bower-components/font-awesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}" />

<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
