<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta name="theme-color" content="#ffffff">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }} @yield('title')</title>

<!-- Favicon section -->
<link rel="icon" type="image/png" href="{{ url('/main_layout/images/favicons/lara.png') }}" sizes="32x32">
<link rel="icon" type="image/png" href="{{ url('/main_layout/images/favicons/lara.png') }}" sizes="16x16">
<link rel="manifest" href="{{ url('/main_layout/images/favicons/lara.json') }}">
<link rel="mask-icon" href="{{ url('/main_layout/images/favicons/lara.svg') }}" color="#073a85">

<!-- Styles -->
@include('layouts._includes.css_new_files')

@stack('styles')

<script>

</script>
