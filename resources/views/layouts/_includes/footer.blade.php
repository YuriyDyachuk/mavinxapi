<div class="container-fluid footer">
    <div id="copyright" class="copyright col-sm-12 col-xs-12 text-center">
        <span>Copyright © Mavinx - PDF 2020-<?php echo date('Y');?> . All right reserved.</span>
    </div>
</div>

@section('scripts')

    <!-- Scripts -->
    <script type="text/javascript" src="{{ url('js/app.js') }}"></script>

    {{-- Bower components js --}}
    <script type="text/javascript" src="{{ url('bower-components/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('bower-components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
@endsection
