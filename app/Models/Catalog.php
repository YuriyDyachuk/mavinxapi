<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Catalog extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'url_path',
    ];

    protected $casts = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function imageable()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
