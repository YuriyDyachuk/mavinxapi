<?php

namespace App\Http\Resources;

use http\Env\Response;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\URL;

class CatalogResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function toArray($request)
    {
        $filepath = public_path('storage/files/').$this['url_path'];

        $data = [
            'id' => $this['id'],
            'name' => $this['name'],
            'preview_image' => URL::asset('/') . 'storage/catalogs/' . $this['imageable']['name'],
            'download_link' => URL::route('catalog-api.show', $this['id']),
        ];

        return $data;

//        return \Illuminate\Support\Facades\Response::download($filepath,'pdf',[
//            'Content-Type' => 'application/pdf',
//        ]);

    }
}
