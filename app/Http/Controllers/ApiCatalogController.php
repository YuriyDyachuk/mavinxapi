<?php

namespace App\Http\Controllers;

use App\Http\Resources\CatalogResource;
use App\Models\Catalog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ApiCatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $catalogs = Catalog::query()->with('imageable')->get()->toArray();

        return CatalogResource::collection($catalogs)->response();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function show(int $id): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $res = Catalog::findOrFail($id);
        $catalog = public_path('storage/files/') . $res->url_path;

        return Response::download($catalog,'PDF',[
            "Content-Type: application/json;charset=utf-8"
        ]);

//        $html = mb_convert_encoding($catalog, 'HTML-ENTITIES', 'UTF-8');
//        $html_decode = html_entity_decode($html);
//
//        return response()->file($html_decode, [
//            'Content-Type' => 'application/pdf;'
//        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
