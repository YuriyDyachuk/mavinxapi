<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Image;
use App\Services\ImagesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $catalogs = Catalog::query()->with('imageable')->get();
        return view('catalogs.index', compact('catalogs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $forms = $request->only('title','image','file');
        $rules = [
            'title' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png|max:2048',
            'file' => 'required',
        ];

        $error = Validator::make($forms,$rules);

        $res = new Catalog;
        $res->name = $request->title;

        if($request->hasFile('file')){
            $file     = $request->file('file');
            $filename = time() . '_' . $file->getClientOriginalName();
            $file->move(public_path('storage/files/'), $filename);
            $res->url_path = $filename;
        }

        $res->save();

        if($request->hasFile('image')){

            $files= $request->file('image');
            ImagesService::save($res, $files, $res->id);
        }


        return redirect()->route('catalogs.index')->with(['success' => 'Каталог успешно создан']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        if (!$id) {
            return back()->withErrors(['msg' => 'Ошибка удаления!'])->withInput();
        }

        $res = Catalog::query()->findOrFail($id);
        $res->imageable()->where('imageable_id',$id)->delete();
        $res->delete();

        return redirect()->back()->with(['success' => 'Каталог успешно удален']);
    }
}
