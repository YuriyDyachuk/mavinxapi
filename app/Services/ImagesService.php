<?php

namespace App\Services;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class ImagesService
{
    /**
     * @param Model $model
     * @param UploadedFile $image
     * @param null $id
     * @return \Illuminate\Database\Eloquent\Builder|Model
     */
    public static function save(Model $model, UploadedFile $image, $id = null)
    {
        $filename = self::move($image);
        $data     = [
            'name'           => $filename,
            'imageable_id'   => $id,
            'imageable_type' => get_class($model),
        ];

        $data['filename'] = $filename;
        $document = Image::query()->create($data);

        return $document;
    }

    /**
     * @param UploadedFile $image
     * @return string
     */
    public static function move(UploadedFile $image)
    {
        $filename = time() . '_' . $image->getClientOriginalName();
        $image->move(public_path('storage/catalogs/'), $filename);

        return $filename;
    }

}
